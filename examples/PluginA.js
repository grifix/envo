(function ($) {
    var parent = envo.Plugin;
    var prototype = envo.makePluginPrototype(
        'demo.PluginA',
        'demo_pluginA',
        parent,
        {
            message: 'Hello world!',
        }
    );
    
    
    prototype.init = function () {
        var that = this;
        
        that.elButton = that.find('button');
        that.elButton.click(function () {
            that.showMessage();
        });
        
        parent.prototype.init.call(that);
    };
    
    prototype.showMessage = function () {
        var that = this;
        alert(that.cfg.message);
        return that.el();
    };
})(jQuery);