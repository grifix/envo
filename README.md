# Envo
Библиотека Envo предназначена для облегчения создания и работы с плагинами jQuery
и решает следующие задачи:
  * Организация кода плагина в виде псевдо-класса
  * Наследование плагинов
  * Передача параметров в плагин
  * Подключение плагинов к странице
  * Ajax-перезагрузка плагинов
  * Подписка на события плагинов
 
> Код всех примеров можно найти в папке **examples** данного репозитория
 
##Подключение библиотеки
Для работы Envo вам понадобится jQuery версии 2.0 и выше.

    <script src="../vendor/components/jquery/jquery.min.js"></script>
    <script src="../src/envo.js"></script>
    <script src="../src/Plugin.js"></script>

 
##Создание простого плагина

Создадим простой плагин, который показывает сообщение при нажатии на кнопку.
Код псевдо-класса плагина будет выглядеть следующим образом

    (function ($) {
        var parent = envo.Plugin; // Плагин от которого наследуется данный плагин
        var prototype = envo.makePluginPrototype(
            'demo.PluginA', // Название псевдо-класса плагина (используется при наследовнии)
            'demo_pluginA', // Имя плагина (используется для подключиня плагина к DOM-элементам)
            parent,
            {
                message: 'Hello world!' //Конфигурация плагина по умолчанию
            }
        );
        
        // Функция которая подключается к плагину в момент наложения его на DOM елемент
        prototype.init = function () {
            var that = this;
            
            that.elButton = that.find('button');
            that.elButton.click(function () {
                that.showMessage();
            });
            
            parent.prototype.init.call(that);
        };
        
        // Метод псевдо-класса плагина
        prototype.showMessage = function () {
            var that = this;
            alert(that.cfg.message);
            return that.el();
        };
    })(jQuery);
    
Теперь создадим DOM-элемент к которому будет подключатся данный плагин:

    <div class="box">
        <button class="plg-demo_pluginA-button">Click Me!</button>
    </div>
    
Подключим плагин к DOM-элементу

    $('div.box').demo_pluginA();
    
Кнопка в примере имеет css-класс **plg-demo_pluginA-button** который состоит
из префикса **plg-**, имени плагина **demo_pluginA** и суффикса с названием елемента
**button**. Такие классы позволяют не привязывать элементы плагина к структуре DOM-элемента
и css-классам, которые отвечают за стиль элемента. Поиск такого элемента внутри класса плагина
осуществляется при помощи метода **find** единственным параметром которого является
суффикс элемента.

    that.elButton = that.find('button');
    
## Передача конфигурационных параметров в плагин
Предположим нам нужно изменить текст сообщения которое появляется при нажатии на кнопку.
Мы можем ето сделать тремя разными способами:

Указать парметры при подключении плагина к элементу:

    $('div.box').demo_pluginA({"message":"Custom message!"});
    
Передать параметры через свойства DOM-элемента

    <div class="plg-demo_pluginA" data-message="Custom message!">
        <button class="plg-demo_pluginA-button">Click Me!</button>
    </div>
    
Задать парамтры после подключения плагина к DOM-элементу
    
    $('div.box').demo_pluginA();
    $('div.box').data('demo_pluginA').setCfg({'messagge':'Custom message!'})
    
Получить доступ к параметрам внутри класса плагина можно с помощью свойтса **cfg**

    var that = this;
    alert(that.cfg.message); 
    
##Автоматическое подключение плагинов к DOM-элементам страницы
В предыдущем примере мы подключали плагины к DOM-элементам вручную при помощи кода

    $('div.box').demo_pluginA();
    
Когда элементов на странице много, такой подход может оказаться не слишком удобным,
поэтому envo дает возможность подключить все плагины скопом в автоматическом
режиме. Для этого к DOM-элементам к которым нужно подключить плагин, нужно добавить
специальный класс, который состоит из префикса **plg-** и имени плагина:

    <div class="plg-demo_pluginA">
        <button class="plg-demo_pluginA-button">Click Me!</button>
    </div>
    
и добавить на станицу код:

     jQuery(function($) {
        envo.attachPlugins();
     });

##Наследование плагинов
Предположим мы хотим расширить функциональность demo_pluginA так чтобы после 
отображения сообщения отобразилось еще и окно с подтверждением. Создадим новый плагин
который наследует demo_pluginA:

    (function ($) {
        var parent = demo.PluginA;
        var prototype = envo.makePluginPrototype(
            'demo.PluginB',
            'demo_pluginB',
            parent
        );
        
        prototype.showMessage = function () {
            var that = this;
            parent.prototype.showMessage.call(that);
            confirm("Are you sure!");
            return that.el();
        };
    })(jQuery);
    
и подключим его к DOM-элементу:

    <div class="plg-demo_pluginB">
        <button class="plg-demo_pluginB-button">Click Me!</button>
    </div>
    
##Ajax-перезагрузка плагинов
Допустим мы хотим создать плагин который отображал бы список с постраничной разбивкой
но при нажатии на ссылку с номером страницы, перезагружалась бы на вся страница, а
только содержимое плагина.

html которому мы будем подключать плагин выглядит так:

    <?php
    $page = 1;
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    }
    $data = [
        1 => [
            'One',
            'Two',
            'Three',
        ],
        2 => [
            'Four',
            'Five',
            'Six',
        ],
        3 => [
            'Seven',
            'Eight',
            'Nine',
        ],
    ];
    $currentPageData = $data[$page];
    $pagesNumbers = array_keys($data);
    ?>
    <div class="plg-demo_pluginD">
        <?php foreach ($currentPageData as $item): ?>
            <div><?= $item ?></div>
        <?php endforeach; ?>
        <div>
            <?php foreach ($pagesNumbers as $pageNumber): ?>
                <button class="plg-demo_pluginD-page"><?= $pageNumber ?></button>
            <?php endforeach; ?>
        </div>
    </div>
    
Код плагина:

    (function ($) {
        var parent = envo.Plugin;
        var prototype = envo.makePluginPrototype(
            'demo.PluginD',
            'demo_pluginD',
            parent
        );
        
        //Информация о том откуда плагин должен загружать html код
        prototype.source={
            "method":"get",
            "url":"server.php?tpl=template1"
        };
        
        prototype.init = function () {
            var that = this;
            
            that.elPage = that.find('page');
            that.elPage.click(function () {
                that.loadPage($(this).text())
            });
            
            parent.prototype.init.call(that);
        };
        
        prototype.loadPage=function(pageIndex){
            var that = this;
            that.reload({
                source:{
                    data:{
                        page:pageIndex
                    }
                }
            });
        }
        
    })(jQuery);
    
В переменной **prototype.source** мы указали параметры ajax-запроса для загрузки html-кода
плагина. 

Метод **reload** позволяет осуществить такую загрузку переопределив нужные параметры
ajax запроса. В нашем случае мы указывает с какой страницы нам нужны данные. После
загрузки плагин автоматически изменит свое содержимое html-кодом который был
получен с сервера и повторно запустит init.

## Coбытия
Плагин может запустить событие при помощи метода **fireEvent**

    prototype.showMessage = function () {
        var that = this;
        alert(that.cfg.message);
        that.fireEvent('showMessage',that.cfg.message)
        return that.el();
    };
    
Подписка на событие осуществляется следующим образом:

    $('div.box').data('demo_pluginA').on('showMessage',function(message){
        console.log(message)
    });

