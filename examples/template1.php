<?php
$page = 1;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
}
$data = [
    1 => [
        'One',
        'Two',
        'Three',
    ],
    2 => [
        'Four',
        'Five',
        'Six',
    ],
    3 => [
        'Seven',
        'Eight',
        'Nine',
    ],
];
$currentPageData = $data[$page];
$pagesNumbers = array_keys($data);
?>
<div class="plg-demo_pluginD">
    <?php foreach ($currentPageData as $item): ?>
        <div><?= $item ?></div>
    <?php endforeach; ?>
    <div>
        <?php foreach ($pagesNumbers as $pageNumber): ?>
            <button class="plg-demo_pluginD-page"><?= $pageNumber ?></button>
        <?php endforeach; ?>
    </div>
</div>
