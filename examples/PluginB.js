(function ($) {
    var parent = demo.PluginA;
    var prototype = envo.makePluginPrototype(
        'demo.PluginB',
        'demo_pluginB',
        parent
    );
    
    prototype.showMessage = function () {
        var that = this;
        parent.prototype.showMessage.call(this);
        confirm("Are you sure!");
        return that.el();
    };
})(jQuery);