(function ($) {
    var parent = envo.Plugin;
    var prototype = envo.makePluginPrototype(
        'demo.PluginD',
        'demo_pluginD',
        parent
    );
    
    prototype.source={
        "method":"get",
        "url":"server.php?tpl=template1",
        "dataType":'json'
    };
    
    prototype.init = function () {
        var that = this;
        
        that.elPage = that.find('page');
        that.elPage.click(function () {
            that.loadPage($(this).text())
        });
        
        parent.prototype.init.call(that);
    };
    
    prototype.loadPage=function(pageIndex){
        var that = this;
        that.reload({
            source:{
                data:{
                    page:pageIndex
                }
            }
        });
    }
    
})(jQuery);