/*
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

var envo = {
    //Info about plugins
    plugins: {
        //Names of registered plugins
        names: [],
        
        //Constructors of registered plugins
        constructors: {}
    }
};

var jQuery;

(function ($) {
    /**
     * Makes new class constructor
     *
     * @param {function} constructor - constructor function
     * @param {string} className - name of class
     * @param {function|Array} parentConstructors - parent constructors
     *
     * @returns {*}
     */
    
    envo.makeConstructor = function (constructor, className, parentConstructors) {
        if (!parentConstructors) {
            parentConstructors = [Object];
        }
        
        if (!$.isArray(parentConstructors)) {
            parentConstructors = [parentConstructors];
        }
        
        var prototype = {};
        
        $.each(parentConstructors, function () {
            var ParentConstructor = this;
            
            //Extend only envo objects (for Bootstrap compatibility)
            if (ParentConstructor.prototype.className) {
                prototype = $.extend(true, prototype, ParentConstructor.prototype);
            }
        });
        
        constructor.prototype = $.extend(true, {}, prototype, constructor.prototype);
        
        constructor.prototype.className = className;
        
        constructor.prototype.parentConstructors = parentConstructors;
        
        //add className to global namespace
        eval(className + '=constructor;');
        
        return constructor;
    };
    
    /**
     * Finds elements by plugin name
     *
     * @param {jQuery} collection jquery collection
     * @param {string} plugName plugin name
     *
     * @return {Array}
     */
    envo.findElementsByPlugin = function (collection, plugName) {
        var result = [];
        $.each(collection, function () {
            if ($(this).data(plugName)) {
                result.push($(this));
            }
        });
        return result;
    };
    
    /**
     * Makes a namespace
     *
     * @param {string} namespace
     *
     * @return {void}
     */
    envo.namespace = function (namespace) {
        var parts = namespace.split('.');
        var box = window;
        $.each(parts, function (i, part) {
            if (!box[part]) {
                box[part] = {};
            }
            box = box[part];
        });
    };
    
    /**
     * Attaches plugins to elements by css-class
     *
     * @param {jQuery} elContainer a container in which you want to attach plugins
     *
     * @return {void}
     */
    envo.attachPlugins = function (elContainer) {
        
        if (!elContainer) {
            elContainer = $('body');
        }
        
        $.each(envo.plugins.names, function (k, pluginName) {
            
            var elements = elContainer.find('.plg-' + pluginName);
            
            elements.each(function () {
                
                var el = $(this);
                
                //if element exists and plugin is not attached
                if (el && el.length && !el.data(pluginName)) {
                    
                    //Attach plugin
                    el[pluginName]({});
                }
            });
        });
    };
    
    /**
     * Register new plugin and makes jquery plugin
     *
     * @param {string} pluginName name of plugin
     * @param {function} pluginConstructor plugin constructor
     *
     * @return {jQuery|*}
     */
    envo.registerPlugin = function (pluginName, pluginConstructor) {
        
        //Makes jquery plugin
        $.fn[pluginName] = function (param) {
            var args = arguments;
            var result = this;
            
            this.each(function () {
                var el = $(this);
                
                //If the first params is an plugin options object, the plugin is not yet attached to element
                if ((typeof param == 'object' || param === undefined) && !el.data(pluginName)) {
                    
                    el.data(pluginName, new pluginConstructor(el, param));
                    
                    try {
                        el.data(pluginName).init();
                    }
                    catch (e) {
                        throw e;
                    }
                    
                }
                //If the first param is a string, run the plugin method which тфьу is equal to this string
                else if (typeof param == 'string') {
                    
                    var plugin = el.data(pluginName);
                    
                    if (!plugin) {
                        throw new Error(pluginName + " was not attached to element!");
                    }
                    
                    var r = plugin[param].apply(plugin, Array.prototype.slice.call(args, 1));
                    
                    //If result is no jquery collection
                    if (r[0] !== el[0]) {
                        result = r;
                        return false;
                    }
                }
            });
            return result;
        };
        
        envo.plugins.names.push(pluginName);
        
        envo.plugins.constructors[pluginName] = pluginConstructor;
    };
    
    /**
     * Makes the plugin constructor
     *
     * @param {string} pluginClassName class name e.g. demo.news.Grid
     * @param {string} pluginName name of plugin e.g. demo_news_grid
     * @param {Array | function} parentConstructors constructors of parent classes
     *
     * @returns {Function}
     */
    envo.makePluginConstructor = function (pluginClassName, pluginName, parentConstructors) {
        
        //Make plugin class namespace
        var arr = pluginClassName.toString().split(".".toString());
        arr.pop();
        envo.namespace(arr.join('.'));
        
        //make plugin constructor
        var constructor = function (el, opt) {
            envo.Plugin.call(this, el, opt);
        };
        eval("constructor=envo.makeConstructor(constructor,pluginClassName,parentConstructors);");
        
        constructor.prototype.name = pluginName;
        
        envo.registerPlugin(pluginName, constructor);
        
        return constructor;
    };
    
    /**
     * Makes ne plugin prototype
     *
     * @param {string} pluginClassName class name e.g. demo.news.Grid
     * @param {string} pluginName pluginName name of plugin e.g. demo_news_grid
     * @param {function} parentConstructors constructors of parent classes
     * @param {object} defaults plugin default options
     *
     * @returns {object}
     */
    envo.makePluginPrototype = function (pluginClassName, pluginName, parentConstructors, defaults) {
        if (!defaults) {
            defaults = {};
        }
        var result = envo.makePluginConstructor(pluginClassName, pluginName, parentConstructors).prototype;
        
        result.defaults = $.extend(true, {}, result.defaults, defaults);
        
        return result;
    };
    
    /**
     * Makes new dom-element with attached plugin
     * @param {Object} p
     *  {string} name pugin name
     *  {object} opt plugin option
     *  {object|string} source plugin source code or ajax jquery conig to get source
     *  {bool|string} extendCurrentSource extend source ajax params
     *      true - extend
     *      'recursive' - extend recursively
     *      false - no exend
     * @param {function} onSuccess runs after attach plugin to dom-element
     */
    envo.makePlugin = function (p, onSuccess) {
        
        if (!p.name) {
            throw new Error("No plugin name!");
        }
        
        if (!onSuccess) {
            throw new Error("Can't make " + p.name + " plugin, onSuccess function is not defined!")
        }
        
        var pluginConstructor = envo.plugins.constructors[p.name];
        
        if (!pluginConstructor) {
            throw new Error('Unknown plugin "' + p.name + '"!')
        }
        
        p = $.extend(true, {
            name: null,
            opt: {},
            extendCurrentSource: 'recursive'
        }, p);
        
        pluginConstructor.prototype.getSource(function (html) {
            var el = $(html);
            eval("el." + p.name + "(p.opt);");
            
            if (p.source) {
                el.data(p.name).setSource(p.source, p.extendCurrentSource);
            }
            
            onSuccess(el);
        }, p.source);
    };
})(jQuery);