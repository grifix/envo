 
##Tworzenie prostego pluginu

Stwórzmy prosty plugin który pokazuję message przy kliknięciu na button:

    (function ($) {
        var parent = envo.Plugin; // Plugin od którego dziedziczy nasz plugin
        var prototype = envo.makePluginPrototype(
            'demo.PluginA', // Nazwa klasy pluginu (jest ptzebna do dzidziczeia)
            'demo_pluginA', // Nazwa pluginu (jet potrzebna do podpięcia pluginu do DOM elementu)
            parent,
            {
                message: 'Hello world!' //Domyślna konfiguracja pluginu
            }
        );
        
        // Odpala się przy nakładaniu pluginu na DOM element
        prototype.init = function () {
            var that = this;
            
            that.elButton = that.find('button');
            that.elButton.click(function () {
                that.showMessage();
            });
            
            parent.prototype.init.call(that);
        };
        
        // metoda klasy pluginu
        prototype.showMessage = function () {
            var that = this;
            alert(that.cfg.message);
            return that.el();
        };
    })(jQuery);
    
Teraz możemy stworzyć DOM-element:

    <div class="box">
        <button class="plg-demo_pluginA-button">Click Me!</button>
    </div>
    
I podpiąć do niego plugin 

    $('div.box').demo_pluginA();
    
Button w przykładzie ma css-klase **plg-demo_pluginA-button** która się składa z prefiksu  
**plg-**, nazwy pluginu **demo_pluginA** oraz sufiksu z imieniem elementu **button**.
Takie klasy pozwalają nie przywiązywać elementy pluginu do struktury DOM-elementu i css-klas
które odpowiadają za styl elementu. Wyszukiwanie takiego elementu odbywa się przy pomocy metody
**find** jedynym parametrem której jest sufiks elementu. 

    that.elButton = that.find('button');
    
## Przekazanie konfiguracji do pluginu
Powiedzmy że chcemy zmienić komunikat który się wyświetla przy kliknięciu na przycisk.
Możemy to zrobić na trzy różne sposoby:

Wskazać parametry przy podłączeniu pluginu do elementu: 

    $('div.box').demo_pluginA({"message":"Custom message!"});
    
Przekazać parametry przy pomocy parametrów DOM-elementu:

    <div class="plg-demo_pluginA" data-message="Custom message!">
        <button class="plg-demo_pluginA-button">Click Me!</button>
    </div>
    
Przekazać parametry po podpięciu plugina do DOM-elementu:
    
    $('div.box').demo_pluginA();
    $('div.box').data('demo_pluginA').setCfg({'messagge':'Custom message!'})
    
Odczytać parametry w pluginie można przy pomocy zmiennej cfg:

    var that = this;
    alert(that.cfg.message); 
    
##Automatyczne podłączenie pluginów do DOM-elementów
W poprzednim przykładzie plugin był podłączany ręcznie:

    $('div.box').demo_pluginA();
    
Ale to nie jest zbyt wygodny sposób kiedy elementów na stronie jest sporo,
dlatego envo daje możliwość podłączyć pluginy do wszyskich elementów na raz.
Żeby to zrobić trzeba nadać DOM elementom do których chcemy podłączyć pluginy
specjalną klasę imię której składa się z prefiksu **plg-** i imienia pluginu.

    <div class="plg-demo_pluginA">
        <button class="plg-demo_pluginA-button">Click Me!</button>
    </div>
    
i dodać na stronę kod:

     jQuery(function($) {
        envo.attachPlugins();
     });

##Dziedziczenie pluginów
Załóżmy że chcemy rozszerzyć funkcjonalność demo_pluginA taż żeby po alercie pokazać jeszcze confirm.
Strwóżmy nowy plugin który dziedziczy od demo_pluginA:

    (function ($) {
        var parent = demo.PluginA;
        var prototype = envo.makePluginPrototype(
            'demo.PluginB',
            'demo_pluginB',
            parent
        );
        
        prototype.showMessage = function () {
            var that = this;
            parent.prototype.showMessage.call(that);
            confirm("Are you sure!");
            return that.el();
        };
    })(jQuery);
    
i podłączymy go do DOM-elementu:

    <div class="plg-demo_pluginB">
        <button class="plg-demo_pluginB-button">Click Me!</button>
    </div>
    
##Ajax-przeładownie pluginów
Załóżmy że chcemy zrobić plugin który pokazuję listę ze stronicowaniem, tak żeby każda strona się
renderowała na serwerze, ale przy kliknięciu na poszczególną stronę przeładowywała się nie cała strona
a tylko część z pluginem.

html będzie renderowny po stronie serwera takim skryptem:

    <?php
    $page = 1;
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    }
    $data = [
        1 => [
            'One',
            'Two',
            'Three',
        ],
        2 => [
            'Four',
            'Five',
            'Six',
        ],
        3 => [
            'Seven',
            'Eight',
            'Nine',
        ],
    ];
    $currentPageData = $data[$page];
    $pagesNumbers = array_keys($data);
    ?>
    <div class="plg-demo_pluginD">
        <?php foreach ($currentPageData as $item): ?>
            <div><?= $item ?></div>
        <?php endforeach; ?>
        <div>
            <?php foreach ($pagesNumbers as $pageNumber): ?>
                <button class="plg-demo_pluginD-page"><?= $pageNumber ?></button>
            <?php endforeach; ?>
        </div>
    </div>
    
Kod pluginu:

    (function ($) {
        var parent = envo.Plugin;
        var prototype = envo.makePluginPrototype(
            'demo.PluginD',
            'demo_pluginD',
            parent
        );
        
        //Informacja o tym skąd plugin ma zassać wyrenderowany html
        prototype.source={
            "method":"get",
            "url":"server.php?tpl=template1"
        };
        
        prototype.init = function () {
            var that = this;
            
            that.elPage = that.find('page');
            that.elPage.click(function () {
                that.loadPage($(this).text())
            });
            
            parent.prototype.init.call(that);
        };
        
        prototype.loadPage=function(pageIndex){
            var that = this;
            that.reload({
                source:{
                    data:{
                        page:pageIndex
                    }
                }
            });
        }
        
    })(jQuery);
    
W zmiennej **prototype.source** podaliśmy parametry ajax-requestu do pobierania html-kodu.

Metoda **reload** pozwala wyrenderować html ponowie z dowolnymi parametrami ajax-requestu.
W naszym przypdku jest przekazywany numer strony którą chcemy wyrenderować.

## Zdarzenia
Plugin może odpalić zdarzenie przy pomocy metody **fireEvent**

    prototype.showMessage = function () {
        var that = this;
        alert(that.cfg.message);
        that.fireEvent('showMessage',that.cfg.message)
        return that.el();
    };
    
Nasłuchiwanie zdarzenia robi się w taki sposób:

    $('div.box').data('demo_pluginA').on('showMessage',function(message){
        console.log(message)
    });

