(function ($) {
    var parent = demo.PluginA;
    var prototype = envo.makePluginPrototype(
        'demo.PluginC',
        'demo_pluginC',
        parent
    );
    prototype.source="<div><button class='plg-demo_pluginC-button'>Click Me!</button></div>";
    
})(jQuery);