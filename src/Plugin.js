/*
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
(function ($) {
    envo.namespace('envo');
    
    /**
     * Plugin constructor
     * @param {jQuery} el an element that connects the plugin
     * @param {Object} opt plugin
     * @constructor
     */
    envo.Plugin = function (el, opt) {
        if (!opt) {
            opt = {};
        }
        
        var that = this;
        
        //Gets plugin options from attribute
        var optFromAttr = el.attr('data-' + that.name + '-opt');
        if(!optFromAttr){
            optFromAttr = el.attr('data-opt');
        }
        
        
        //If attribute has options
        if (optFromAttr) {
            opt = $.extend(true, {}, $.parseJSON(optFromAttr), opt);
        }
        else{
            opt = $.extend(true, {}, el.data(), opt);
        }
        
        that.cfg = $.extend(true, {}, that.cfg, opt);
        
        
        //Protect options from changes
        that.opt = function () {
            return opt;
        };
        
        //Protect element from changes
        that.el = function () {
            return el;
        };
        
        that.defineCfg();
        
    };
    
    var constructor = envo.makeConstructor(envo.Plugin, 'envo.Plugin', Object);
    var prototype = constructor.prototype;
    
    /**
     * Name of plugin
     *
     * @type {string}
     */
    prototype.name = null;
    
    /**
     * Default options
     *
     * @type {Object}
     */
    prototype.defaults = {};
    
    /**
     * Default source code
     *
     * @type {string}
     */
    prototype.source = '<div></div>';
    
    /**
     * Configuration
     * @type {Object}
     */
    prototype.cfg = {
        on: {
            init: function () {
                
            }
        }
    };
    
    
    /**
     * Runs on plugin attaches to elements
     */
    prototype.init = function () {
        var that = this;
        
        if (!that.cfg.on) {
            that.cfg.on = {};
        }
        
        $.each(that.cfg.on, function (eventName, handlers) {
            
            if (handlers instanceof Function) {
                that.cfg.on[eventName] = [handlers];
            }
        });
        
        envo.attachPlugins(that.el());
        that.fireEvent('init');
    };
    
    /**
     * Returns the element that was connected to the plug-in
     */
    prototype.el = function () {
    };
    
    /**
     * Returns plugin initialization options
     */
    prototype.opt = function () {
    };
    
    /**
     * Returns plugin child-element
     *
     * @param {string} name name of element ()
     *
     * @returns {jQuery}
     */
    prototype.find = function (name) {
        var that = this;
        return that.el().find(that.makeSelector(name));
    };
    
    /**
     * Makes selector for plugin child-element
     *
     * @param {string} name element class name suffix e.g. button for plg-demo_Grid-button)
     *
     * @returns {string}
     */
    prototype.makeSelector = function (name) {
        var that = this;
        return '.' + that.makeCssClass() + '-' + name;
    };
    
    /**
     * Makes plugin css class
     *
     * @return {string}
     */
    prototype.makeCssClass = function () {
        var that = this;
        return 'plg-' + that.name;
    };
    
    
    /**
     * Makes plugin default configuration
     *
     * @param {Object} defaults
     */
    prototype.defineCfg = function (defaults) {
        var that = this;
        if (!defaults) {
            defaults = {};
        }
        that.cfg = $.extend(true, {}, that.defaults, defaults, that.cfg);
    };
    
    /**
     * fire jquery event
     *
     * @param {string} eventName
     * @param {object} params
     *
     * @return {jQuery}
     */
    prototype.trigger = function (eventName, params) {
        var that = this;
        that.el().trigger(that.name + '.' + eventName, params);
        return that.el();
    };
    
    /**
     * fire plugin event
     *
     * @param {string} eventName
     * @param {object} params
     *
     * @return {jQuery}
     */
    prototype.fireEvent = function (eventName, params) {
        var that = this;
        
        if (that.cfg.on[eventName]) {
            $.each(that.cfg.on[eventName], function (k, handler) {
                handler.call(that, params);
            });
        }
        return that.el();
    };
    
    /**
     * Attach event handler
     *
     * @param {string} eventName
     * @param {function} handler
     *
     * @return {Object}
     */
    prototype.on = function (eventName, handler) {
        var that = this;
        if (!that.cfg.on[eventName]) {
            that.cfg.on[eventName] = [];
        }
        else if (!Array.isArray(that.cfg.on[eventName])) {
            that.cfg.on[eventName] = [that.cfg.on[eventName]];
        }
        that.cfg.on[eventName].push(handler)
        return that;
    };
    
    /**
     * Sets plugin html source
     *
     * @param {string|Object} source html-code of ajax config to get source from server
     * @param {bool|string} extend extend current source (true, false, 'recursive')
     *
     * @return {void}
     */
    prototype.setSource = function (source, extend) {
        var that = this;
        var recursive = false;
        if (extend == 'recursive') {
            recursive = true;
        }
        if ((source instanceof Object) && extend) {
            that.source = $.extend(recursive, {}, that.source, source);
        }
        else {
            that.source = source
        }
    };
    
    /**
     * Sets plugin configuration
     *
     * @param {Object} cfg
     * @param {bool|string} extend extend current configuration (true, false, 'recursive')
     */
    prototype.setCfg = function (cfg, extend) {
        var that = this;
        var recursive = false;
        if (extend == 'recursive') {
            recursive = true;
        }
        if (extend) {
            that.cfg = $.extend(recursive, {}, that.cfg, cfg);
        }
        else {
            that.cfg = cfg;
        }
    };
    
    /**
     * Gets plugin html source
     * @param {function} handler
     * @param {Object|string|null} extendedSource ajax query config or html
     */
    prototype.getSource = function (handler, extendedSource) {
        var that = this;
        
        if ((that.source instanceof Object) || (extendedSource instanceof Object)) {
            if (!extendedSource) {
                extendedSource = {};
            }
            
            if (that.source instanceof Object) {
                extendedSource = $.extend(true, {}, that.source, extendedSource)
            }
            
            var success = extendedSource.success;
            extendedSource.success = function (data) {
                if (success) {
                    success.call(this, data);
                }
                handler.call(that, data);
                
            };
            $.ajax(extendedSource);
        }
        else {
            if (!extendedSource) {
                extendedSource = that.source;
            }
            if (!extendedSource) {
                throw new Error('No plugin source!')
            }
            handler.call(that, extendedSource);
        }
    };
    
    /**
     * Reloads plugin (reload html and init plugin)
     *
     * @param {Object} p
     *      {Object} opt plugin options
     *      {string|Object} source html code or ajax config
     *      {bool} extendCurrentSource true false 'recursive'
     * @param handler
     */
    prototype.reload = function (p, handler) {
        
        var that = this;
        
        var def = {
            source: that.source,
            extendCurrentSource: 'recursive',
            extendOpt: 'recursive'
        };
        
        p = $.extend(true, {}, def, p);
        
        
        that.getSource(function (data) {
        
            that.el().html($(data).html());
            
            if (p.source) {
                that.setSource(p.source, p.extendCurrentSource)
            }
            
            if (p.opt) {
                that.setCfg(p.opt, p.extendOpt);
            }
            
            that.init();
            
            envo.attachPlugins(that.el());
            
            if (handler) {
                handler.call(that);
            }
            
        }, p.source)
    };
    
    
})(jQuery);