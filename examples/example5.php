<?php
/**
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Server source</title>
    <script src="../vendor/components/jquery/jquery.min.js"></script>
    <script src="../src/envo.js"></script>
    <script src="../src/Plugin.js"></script>
    <script src="PluginD.js"></script>
</head>
<body>
<script>
    jQuery(function($) {
        envo.attachPlugins();
    });
</script>

<?php include ('template1.php')?>

</body>
</html>
